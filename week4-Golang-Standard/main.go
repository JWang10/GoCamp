package main

import (
	"context"
	"fmt"
	"golang.org/x/sync/errgroup"
	"log"
	"net/http"
	"os"
	"os/signal"
	"week4-Golang-Standard/internal/biz"
	"week4-Golang-Standard/internal/data"
	"week4-Golang-Standard/internal/service"
)

/*
$ curl http://localhost:8080/
GoCamp
$ curl http://localhost:8080/test
Tom (from DB)

--logs
Http server start at  :8080
2022/07/26 17:53:23
2022/07/26 17:53:23 1 Tom 20
*/

var userService *service.UserService

func init() {
	db := data.NewDB()
	userRepo := data.NewUserRepo(db)
	userBiz := biz.NewUserBiz(userRepo)
	userService = service.NewUserService(userBiz)
}
func main() {
	// Parameters
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx) // call WithCancel() -> cancel() to close the context
	eg, egCtx := errgroup.WithContext(ctx) // errgroup control goroutine
	port := ":8080"
	server := &http.Server{Addr: port}

	// listen linux signal
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt) // =syscall.SIGINT

	// Start http server
	eg.Go(func() error {
		return StartHttpServer(server)
	})
	// 可以獨立出來，也可以把這段放到監聽 egCtx chan
	eg.Go(func() error {
		// block until receiving the signal of errCtx was closed
		<-egCtx.Done() // cancel, timeout, deadline 都會觸發 Done
		fmt.Println()
		fmt.Println("Http server stop")
		return server.Shutdown(ctx)
	})
	eg.Go(func() error {
		// listen signal: ctrl+c or kill -9
		for {
			select {
			case <-egCtx.Done():
				return egCtx.Err()
			case <-sig: // ctrl+c or kill -9
				cancel()
			}
		}
	})

	// blocks until all function calls from the Go method have returned
	if err := eg.Wait(); err != nil {
		fmt.Println("error: ", err)
	} else {
		fmt.Println("done successfully")
	}
}

func StartHttpServer(server *http.Server) error {
	// handle part need to move /api
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte("GoCamp"))
		if err != nil {
			return
		}
	})
	http.HandleFunc("/test", func(w http.ResponseWriter, r *http.Request) {
		ids := r.URL.Query().Get("test")
		log.Println(ids)
		if len(ids) < 0 {
			log.Println("user not found")
			return
		}
		//id, _ := strconv.Atoi(ids)
		user, _ := userService.GetUserName(int64(1)) // for db, first data : Jason
		w.Write([]byte(user))
	})
	fmt.Println("Http server start at ", server.Addr)
	err := server.ListenAndServe()
	return err
}
