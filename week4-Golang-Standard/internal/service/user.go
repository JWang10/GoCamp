package service

import (
	"context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"week4-Golang-Standard/internal/api"
	"week4-Golang-Standard/internal/biz"
)

type UserService struct {
	biz *biz.UserBiz
}

func NewUserService(biz *biz.UserBiz) *UserService {
	return &UserService{biz: biz}
}

// GetUsers http part
func (u *UserService) GetUsers() ([]string, error) {
	users, err := u.biz.GetUsers()
	return users.NameList, err
}

// GetUser http part
func (u *UserService) GetUser(id int64) (string, error) {
	user, err := u.biz.GetUser(uint64(id))
	return user.Name, err
}

// GetUserName http part
func (u *UserService) GetUserName(id int64) (string, error) {
	user, err := u.biz.GetUserName(uint64(id))
	return user.Name, err
}

// UsersInfo rpc part
func (u *UserService) UsersInfo(ctx context.Context, request *api.UserInfoRequest) (*api.UserInfoReply, error) {
	user, err := u.biz.GetUser(request.Uid)
	if err != nil {
		// 如果這裡是內部錯誤，要記得包裝，不然就會暴露
		return nil, status.Error(codes.Code(1), "error")
	}
	return &api.UserInfoReply{
		User: &api.User{
			Name: user.Name,
		},
	}, nil
}
