package biz

import "week4-Golang-Standard/internal/data"

type UserBiz struct {
	repo *data.UserRepo
}

func NewUserBiz(repo *data.UserRepo) *UserBiz {
	return &UserBiz{repo: repo}
}

func (u *UserBiz) GetUser(id uint64) (*UserDO, error) {
	user, err := u.repo.GetUser(id)
	if err != nil {
		return &UserDO{}, err
	}
	return &UserDO{Name: user.Name}, nil
}

func (u *UserBiz) GetUserName(id uint64) (*UserDO, error) {
	user, err := u.repo.GetUserName(id)
	if err != nil {
		return &UserDO{}, err
	}
	return &UserDO{Name: user.Name}, nil
}

func (u *UserBiz) GetUsers() (*UserDO, error) {
	users, err := u.repo.GetUsers()
	if err != nil {
		return &UserDO{}, err
	}
	return &UserDO{NameList: users}, nil
}

type UserDO struct {
	Name     string
	NameList []string
}
