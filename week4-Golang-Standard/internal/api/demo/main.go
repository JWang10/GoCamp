package main

import (
	"fmt"
)

func main() {
	user := &User{Name: "Jack", Age: 32}
	fmt.Printf("%+v\n", user)

	tName := user.GetName()
	tAge := user.GetAge()
	fmt.Println(tName, tAge)
}
