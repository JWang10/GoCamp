package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"time"
)

var Db *sql.DB

func main() {
	db, err := sql.Open("mysql", "devauth:devauth@/testDb")
	if err != nil {
		fmt.Println("DB open fail")
		panic(err.Error())
	}
	defer func(db *sql.DB) {
		err := db.Close()
		if err != nil {

		}
	}(db)

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	// Open doesn't open a connection. Validate DSN data:
	err = db.Ping()
	if err != nil {
		fmt.Println("DB connect fail")
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	Db = db
	Query()
	QueryName(1)
}

func QueryName(inputId int64) {
	db := Db
	var name string
	var id, age int
	res := db.QueryRow("SELECT * FROM user_info WHERE id = ?", inputId)

	err := res.Scan(&id, &name, &age)
	if err != nil {
		fmt.Println(err.Error())
	}
	user := &UserPO{
		Id:   id,
		Name: name,
		Age:  age,
	}
	fmt.Println(id, name, age)
	fmt.Printf("%v", user)
}

func Query() {
	db := Db
	rows, err := db.Query("SELECT * FROM user_info")
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	// Get column names
	columns, err := rows.Columns()
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	// Make a slice for the values
	values := make([]sql.RawBytes, len(columns))

	// rows.Scan wants '[]interface{}' as an argument, so we must copy the
	// references into such a slice
	// See http://code.google.com/p/go-wiki/wiki/InterfaceSlice for details
	scanArgs := make([]any, len(values))
	for i := range values {
		scanArgs[i] = &values[i]
	}

	// Fetch rows
	for rows.Next() {
		// get RawBytes from data
		err = rows.Scan(scanArgs...)
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}

		// Now do something with the data.
		// Here we just print each column as a string.
		var value string
		for i, col := range values {
			// Here we can check if the value is nil (NULL value)
			if col == nil {
				value = "NULL"
			} else {
				value = string(col)
			}
			fmt.Println(columns[i], ": ", value)
		}
		fmt.Println("-----------------------------------")
	}
	if err = rows.Err(); err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
}

type UserPO struct {
	Id   int
	Name string
	Age  int
}
