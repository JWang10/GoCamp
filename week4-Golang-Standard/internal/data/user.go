package data

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
)

//type UserRepo struct {
//	db    *egorm.Component
//	cache *eredis.Component
//}

type UserRepo struct {
	db *Component
}
type DB struct {
	dsn      string
	username string
	password string
}

func NewUserRepo(db *Component) *UserRepo {
	return &UserRepo{
		db,
	}
}

func (u *UserRepo) GetUser(id uint64) (*UserPO, error) {
	return &UserPO{
		Name: "Jason",
	}, nil
}
func (u *UserRepo) GetUserName(inputId uint64) (*UserPO, error) {
	var name string
	var id, age int

	// without orm
	res := u.db.myDB.QueryRow("SELECT * FROM user_info WHERE id = ?", inputId)

	err := res.Scan(&id, &name, &age)
	if err != nil {
		fmt.Println(err.Error())
	}
	log.Println(id, name, age)
	//user := &UserPO{
	//	Id:   id,
	//	Name: name,
	//	Age:  age,
	//}
	return &UserPO{
		Name: name,
	}, nil
}

func (u *UserRepo) GetUsers() ([]string, error) {
	//var nameList []string
	nameList := make([]string, 0)
	// without orm
	db := u.db.myDB
	rows, err := db.Query("SELECT * FROM user_info")
	if err != nil {
		return nil, err
	}

	// Get column names
	columns, err := rows.Columns()
	if err != nil {
		return nil, err
	}

	// Make a slice for the values
	values := make([]sql.RawBytes, len(columns))

	// rows.Scan wants '[]interface{}' as an argument, so we must copy the
	// references into such a slice
	// See http://code.google.com/p/go-wiki/wiki/InterfaceSlice for details
	scanArgs := make([]any, len(values))
	for i := range values {
		scanArgs[i] = &values[i]
	}

	// Fetch rows
	for rows.Next() {
		// get RawBytes from data
		err = rows.Scan(scanArgs...)
		if err != nil {
			return nil, err
		}

		// Now do something with the data.
		// Here we just print each column as a string.
		var value string
		for i, col := range values {
			// Here we can check if the value is nil (NULL value)
			if col == nil {
				value = "NULL"
			} else {
				value = string(col)
				if columns[i] == "name" {
					nameList = append(nameList, value)
				}
			}
			fmt.Println(columns[i], ": ", value)
		}
		fmt.Println("-----------------------------------")
	}
	if err = rows.Err(); err != nil {
		return nil, err
		//panic(err.Error()) // proper error handling instead of panic in your app
	}
	return nameList, nil
}

type UserPO struct {
	Name     string
	NameList []string
}

type UserDO struct {
	Name string
}

func convert() {
	bytes, _ := json.Marshal(&UserPO{})
	do := &UserDO{}
	_ = json.Unmarshal(bytes, do)
}
