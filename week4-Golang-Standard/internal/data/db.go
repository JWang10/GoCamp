package data

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"time"
)

//
//func NewDB() *egorm.Component {
//  //return egorm.Load("mysql.geekbang").Build()
//	return &egorm.Component{}
//}
//
//func NewCache() *eredis.Component {
//	return &eredis.Component{}
//}

type Component struct {
	myDB *sql.DB
}

func NewDB() *Component {
	// hardcore for test, devauth:devauth@/testDb
	db, err := sql.Open("mysql", "devauth:devauth@tcp(127.0.0.1:3306)/testDb")
	if err != nil {
		fmt.Println("DB open fail")
		panic(err.Error())
	}
	//defer func(db *sql.DB) {
	//	err := db.Close()
	//	if err != nil {
	//
	//	}
	//}(db)

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	err = db.Ping()
	if err != nil {
		fmt.Println("DB connect fail")
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	return &Component{db}
}
