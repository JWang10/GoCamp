# API 工程化

把之前的`http server`結合`mysql`，按照`standard google layout`，實際按照工程化下去分配

尚未加上`wire` DI 注入部分
尚未採用`ORM`框架部分

```go
//先在main init()實現初始化部分
func init() {
db := data.NewDB()
userRepo := data.NewUserRepo(db)
userBiz := biz.NewUserBiz(userRepo)
userService = service.NewUserService(userBiz)
}
```

## 目錄結構

![img.png](assets/tree.png)

## Demo

```go
$ curl http://localhost:8080/
GoCamp
$ curl http://localhost:8080/test
Tom (from DB)

--logs
Http server start at  :8080
2022/07/26 17:53:23
2022/07/26 17:53:23 1 Tom 20
--- DB
+----+------+------+
| id | name | age  |
+----+------+------+
|  1 | Tom  | 20   |
+----+------+------+
```