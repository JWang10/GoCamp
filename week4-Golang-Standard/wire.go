package main

import (
	"week4-Golang-Standard/internal/service"
)

func InitUserService() *service.UserService {
	//wire.Build(service.NewUserService, biz.NewUserBiz, data.NewUserRepo, data.NewDB, data.NewCache)
	//wire.Build(service.NewUserService, biz.NewUserBiz, data.NewUserRepo, data.NewDB)
	return &service.UserService{}
}
