package main

import (
	"fmt"
	"golang.org/x/sync/errgroup"
	"log"
	"net/http"
)

func main() {
	eg := errgroup.Group{}
	eg.TryGo(func() error {
		return getPage("https://google.com")
	})
	eg.TryGo(func() error {
		return getPage("https://tw.yahoo.com1")
	})
	if err := eg.Wait(); err != nil {
		log.Fatalf("get error: %v", err)
	}
}

func getPage(url string) error {
	res, err := http.Get(url)
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusOK {
		return fmt.Errorf("fail to get page: %s, wrong statusCode: %d\n", url, res.StatusCode)
	}
	log.Printf("get page %s successfully", url)
	return nil
}
