# errgroup

`errgroup`組合`waitgroup`、`context`、`sync.Once`3個組件，可以輸出第一個發生error的goroutine。

```go
type Group struct {
    cancel func() // 这个存的是context的cancel方法
    wg sync.WaitGroup 
    errOnce sync.Once // 只接受一次error
    err     error // 保存第一個返回的error
}
```

- `Wait`：透過`sync.WaitGroup`的`Wait()`來實現阻塞等待
- `WithContext`：通過`context.WithCancel(ctx)`返回`cancel()`來取消相關的child context的goroutine
- `Go`：透過`sync.Once`實現一次`cancel`操作，並保存第一個返回error
- `TryGo`：跟`Go`差別於，`goroutine`數量低於當前配置，才會進行新調用
- `SetLimit`：限制可用`goroutine`數量

## Demo

![img.png](assets/img.png)

- 多個error，只會呈現第一筆error
  - 因為底層採用`sync.Once`
![2errors.png](assets/2errors.png)