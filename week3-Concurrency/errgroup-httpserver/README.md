# errgroup-httpserver

## 問題

基于 errgroup 实现一个 http server 的启动和关闭 ，以及 linux signal 信号的注册和处理，要保证能够一个退出，全部注销退出。

## 問題拆分

- `http server`啟動
- 基於`errgruop`進行`server`的啟動關閉，支援多個`goroutine`退出
- 實現`linux signal`的註冊和處理，支持 `kill -9 `或` Ctrl+C` 的中斷操作

## 實現

### http server啟動和關閉

一般server啟動

```go
http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte("GoCamp"))
		if err != nil {
			return
		}
	})
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatalln("ListenAndServe error: ", err)
	}
```

`http.ListenAndServe`最後調用的是`func (srv *Server) ListenAndServe() error{...}`，我們可以直接調用`serv * Server`。

```go
//http/server.go
func (srv *Server) ListenAndServe() error {
	if srv.shuttingDown() { 
		return ErrServerClosed
	}
	addr := srv.Addr
	if addr == "" {
		addr = ":http"
	}
	ln, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}
	return srv.Serve(ln)
}
------
//main.go
server = &http.Server{Addr: ":8080"}
if err := server.ListenAndServe(); err != nil {
  log.Fatalln("ListenAndServe error: ", err)
}
err := server.Shutdown(context.TODO())
if err != nil {
  log.Println("server shutdown error: ", err)
  return
}
```

### 使用channel進行中斷操作的註冊和處理

```go
// listen linux signal
sig := make(chan os.Signal, 1)
signal.Notify(sig, os.Interrupt) // =syscall.SIGINT
...
// block until the signal is received 
sigs := <-sig
fmt.Println("signal trigger")
```

### 使用errgroup+context進行goroutine管理

`errgroup`組合`waitgroup`、`context`、`sync.Once`3個組件，可以輸出第一個發生error的goroutine。

```go
type Group struct {
    cancel func() // 这个存的是context的cancel方法
    wg sync.WaitGroup 
    errOnce sync.Once // 只接受一次error
    err     error // 保存第一個返回的error
}
```

- `Wait`：透過`sync.WaitGroup`的`Wait()`來實現阻塞等待
- `WithContext`：通過`context.WithCancel(ctx)`返回`cancel()`來取消相關的child context的goroutine
- `Go`：透過`sync.Once`實現一次`cancel`操作，並保存第一個返回error
- `TryGo`：跟`Go`差別於，`goroutine`數量低於當前配置，才會進行新調用
- `SetLimit`：限制可用`goroutine`數量

```go
eg := errgroup.Group{}
eg.TryGo(func() error {
  return getPage("https://google.com1")
})
eg.TryGo(func() error {
  return getPage("https://tw.yahoo.com1")
})
if err := eg.Wait(); err != nil {
  log.Fatalf("get error: %v", err)
}
--- logs
2022/07/17 21:44:08 get error: Get "https://google.com": dial tcp: lookup google.com: no such host

```

## Demo

![img.png](assets/demo.png)
