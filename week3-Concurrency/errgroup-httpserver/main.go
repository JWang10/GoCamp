package main

import (
	"context"
	"fmt"
	"golang.org/x/sync/errgroup"
	"net/http"
	"os"
	"os/signal"
)

func main() {
	// Parameters
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx) // call WithCancel() -> cancel() to close the context
	eg, egCtx := errgroup.WithContext(ctx) // errgroup control goroutine
	port := ":8080"
	server := &http.Server{Addr: port}

	// listen linux signal
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt) // =syscall.SIGINT

	// Start http server
	eg.TryGo(func() error {
		return StartHttpServer(server)
	})
	// 可以獨立出來，也可以把這段放到監聽 egCtx chan
	eg.TryGo(func() error {
		// block until receiving the signal of errCtx was closed
		<-egCtx.Done() // cancel, timeout, deadline 都會觸發 Done
		fmt.Println()
		fmt.Println("Http server stop")
		return server.Shutdown(ctx)
	})
	eg.TryGo(func() error {
		// listen signal: ctrl+c or kill -9
		for {
			select {
			case <-egCtx.Done():
				//fmt.Println("Http server stop")
				//err := server.Shutdown(ctx)
				//if err != nil {
				//	return err
				//}
				return egCtx.Err()
			case <-sig: // ctrl+c or kill -9
				cancel()
			}
		}
	})

	// blocks until all function calls from the Go method have returned
	if err := eg.Wait(); err != nil {
		fmt.Println("error: ", err)
	} else {
		fmt.Println("done successfully")
	}
}

func StartHttpServer(server *http.Server) error {

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte("GoCamp"))
		if err != nil {
			return
		}
	})
	fmt.Println("Http server start at ", server.Addr)
	err := server.ListenAndServe()
	return err
}
