# Redis Benchmark

## Question

- 使用 redis benchmark 工具, 测试 10 20 50 100 200 1k 5k 字节 value 大小，redis get set 性能。
- 写入一定量的 kv 数据, 根据数据大小 1w-50w 自己评估, 结合写入前后的 info memory 信息 , 分析上述不同 value 大小下，平均每个 key 的占用内存空间。

## Prerequisite

- Redis-benchmark 參數

  ```shell
  Usage: redis-benchmark [-h <host>] [-p <port>] [-c <clients>] [-n <requests>] [-k <boolean>]
  
   -h <hostname>      Server hostname (default 127.0.0.1)
   -p <port>          Server port (default 6379)
   -s <socket>        Server socket (overrides host and port)
   -a <password>      Password for Redis Auth
   --user <username>  Used to send ACL style 'AUTH username pass'. Needs -a.
   -c <clients>       Number of parallel connections (default 50)
   -n <requests>      Total number of requests (default 100000)
   -d <size>          Data size of SET/GET value in bytes (default 3)
   --dbnum <db>       SELECT the specified db number (default 0)
   --threads <num>    Enable multi-thread mode.
   --cluster          Enable cluster mode.
   --enable-tracking  Send CLIENT TRACKING on before starting benchmark.
   -k <boolean>       1=keep alive 0=reconnect (default 1)
   -r <keyspacelen>   Use random keys for SET/GET/INCR, random values for SADD,
                      random members and scores for ZADD.
    Using this option the benchmark will expand the string __rand_int__
    inside an argument with a 12 digits number in the specified range
    from 0 to keyspacelen-1. The substitution changes every time a command
    is executed. Default tests use this to hit random keys in the
    specified range.
   -P <numreq>        Pipeline <numreq> requests. Default 1 (no pipeline).
   -q                 Quiet. Just show query/sec values
   --precision        Number of decimal places to display in latency output (default 0)
   --csv              Output in CSV format
   -l                 Loop. Run the tests forever
   -t <tests>         Only run the comma separated list of tests. The test
                      names are the same as the ones produced as output.
   -I                 Idle mode. Just open N idle connections and wait.
   --tls              Establish a secure TLS connection.
   --sni <host>       Server name indication for TLS.
   --cacert <file>    CA Certificate file to verify with.
   --cacertdir <dir>  Directory where trusted CA certificates are stored.
                      If neither cacert nor cacertdir are specified, the default
                      system-wide trusted root certs configuration will apply.
   --insecure         Allow insecure TLS connection by skipping cert validation.
   --cert <file>      Client certificate to authenticate with.
   --key <file>       Private key file to authenticate with.
   --tls-ciphers <list> Sets the list of prefered ciphers (TLSv1.2 and below)
                      in order of preference from highest to lowest separated by colon (":").
                      See the ciphers(1ssl) manpage for more information about the syntax of this string.
   --tls-ciphersuites <list> Sets the list of prefered ciphersuites (TLSv1.3)
                      in order of preference from highest to lowest separated by colon (":").
                      See the ciphers(1ssl) manpage for more information about the syntax of this string,
                      and specifically for TLSv1.3 ciphersuites.
   --help             Output this help and exit.
   --version          Output version and exit.
  ```

![img.png](assets/help.png)
- 透過`redis-benchmark`工具參數`-d`,即可測試不同字節在redis上的性能
- 事先`redis server`情況
![img.png](assets/redis-server-pre.png)

## Demo

- 測試環境
  - 主機 m1 MacBook Pro
  - Redis server 6.2.6 in docker 
- 使用 `50 clients`， `100000 requests`進行測試

```shell
# 10 bytes
SET: 147058.83 requests per second, p50=0.167 msec                    
GET: 147275.41 requests per second, p50=0.175 msec   
# 20 bytes
SET: 132626.00 requests per second, p50=0.175 msec                    
GET: 141242.94 requests per second, p50=0.175 msec    
# 50 bytes
SET: 144927.53 requests per second, p50=0.167 msec                    
GET: 147492.62 requests per second, p50=0.175 msec  
# 100 bytes
SET: 146842.88 requests per second, p50=0.167 msec                    
GET: 151745.08 requests per second, p50=0.167 msec                    
# 200 bytes
SET: 141242.94 requests per second, p50=0.175 msec                    
GET: 143266.47 requests per second, p50=0.175 msec   
# 1 KB
SET: 147058.83 requests per second, p50=0.175 msec                    
GET: 152439.02 requests per second, p50=0.167 msec     
# 5 KB
SET: 131752.31 requests per second, p50=0.183 msec                    
GET: 136986.30 requests per second, p50=0.191 msec   
```

- 透過`watch`指令即使觀看`redis info`中的`used_memory`字段，方可了解`memory`當下的使用變化

- 使用 `50 clients`和 寫入`30 MB`數據

```shell
# ordinary 
used_memory:930744  (後來刪除了bigkey，會在少一些)
# 10 bytes (=30 MB/50/10=60,000 requests)
SET: 142180.09 requests per second, p50=0.167 msec 
used_memory:929736 
# 20 bytes (=30 MB/50/20=30,000 requests)
SET: 125000.00 requests per second, p50=0.175 msec 
used_memory:929744 
# 50 bytes (=30 MB/50/50=12,000 requests)
SET: 111111.11 requests per second, p50=0.191 msec 
used_memory:929776
# 100 bytes (=30 MB/50/100=6,000 requests)
SET: 89552.23 requests per second, p50=0.279 msec    
used_memory:929832
# 1 KB (=30 MB/50/1000=600 requests)
SET: 42857.14 requests per second, p50=0.543 msec 
used_memory:930744
# 5 KB (=30 MB/50/5000=120 requests)
SET: 24000.00 requests per second, p50=0.975 msec  
used_memory:934840 

# ----
由上述測試可以發現，同樣大小檔案下，當每次傳送字節字段越多，佔用的`memory`也越多
拿`100 bytes`和`5 kb`來看
- 929832 - 929776 = 56 bytes
- 934840 - 930744 = 4096 bytes
```



