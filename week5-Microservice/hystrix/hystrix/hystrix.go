package hystrix

import (
	"fmt"
	"log"
	"sync"
	"time"
)

type RollingWindow struct {
	sync.RWMutex
	buckets        []*Bucket
	size           int     // window size
	failThreshold  float64 // 熔斷觸發的請求失敗率閾值
	totalThreshold int     // 熔斷觸發的請求次數閾值
	lastBreakTime  time.Time
	brokenStatus   bool          // 熔斷狀態
	brokenDuration time.Duration // 熔斷持續時間
}

func NewRollingWindow(
	size int,
	totalThreshold int,
	failThreshold float64,
	brokenDuration time.Duration,
) *RollingWindow {
	return &RollingWindow{
		buckets:        make([]*Bucket, 0, size),
		size:           size,
		failThreshold:  failThreshold,
		totalThreshold: totalThreshold,
		brokenDuration: brokenDuration,
	}
}

func (r *RollingWindow) AddBucket() {
	r.Lock()
	defer r.Unlock()
	r.buckets = append(r.buckets, NewBucket())
	// discard first bucket in queue if queue is full
	if !(len(r.buckets) < r.size+1) {
		r.buckets = r.buckets[1:]
	}
}

func (r *RollingWindow) GetBucket() *Bucket {
	if len(r.buckets) == 0 {
		r.AddBucket()
	}
	return r.buckets[len(r.buckets)-1]
}

// GetBrokenStatus 獲取當前熔斷狀態
func (r *RollingWindow) GetBrokenStatus() bool {
	return r.brokenStatus
}

// for testing
func (r *RollingWindow) SetBrokenStatus() {
	r.Lock()
	r.brokenStatus = true
	r.Unlock()
}

// Start
func (r *RollingWindow) Start() {
	go func() {
		for {
			r.AddBucket()
			time.Sleep(100 * time.Millisecond) // add a bucket in 100 ms
		}
	}()
}

// CheckBreak 根據當前滑動窗口檢查是否熔斷
func (r *RollingWindow) CheckBreak() bool {
	r.RLock()
	defer r.RUnlock()
	total := 0
	fail := 0

	for _, val := range r.buckets {
		total += val.Total
		fail += val.Failed
	}

	if total > r.totalThreshold && float64(fail)/float64(total) > r.failThreshold {
		return true
	}
	return false
}

func (r *RollingWindow) ShowAllBuckets() {
	for _, val := range r.buckets {
		fmt.Printf("%s: | total: %d | fail: %d\n",
			val.Timestamp.Format("2006-01-02T15:04:05Z07:00"),
			val.Total,
			val.Failed)
	}
}

// ShowBrokenStatus 每一秒顯示當前是否為熔斷狀態
func (r *RollingWindow) ShowBrokenStatus() {
	go func() {
		for {
			log.Println(r.brokenStatus)
			time.Sleep(time.Second)
		}
	}()
}

// Monitor 監控滑動窗口次數是否熔斷
func (r *RollingWindow) Monitor() {
	go func() {
		for {
			// 熔斷恢復
			if r.brokenStatus && r.OverBrokenDuration() {
				r.Lock()
				r.brokenStatus = false
				r.Unlock()
				//continue
			}
			if r.CheckBreak() {
				r.Lock()
				r.brokenStatus = true
				r.lastBreakTime = time.Now()
				r.Unlock()
			}
		}
	}()
}

func (r *RollingWindow) OverBrokenDuration() bool {
	return time.Since(r.lastBreakTime) > r.brokenDuration
}

func (r *RollingWindow) SaveReqResult(result bool) {
	fmt.Println("SaveReqResult", result)
	r.GetBucket().Record(result)
}
