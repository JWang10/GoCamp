package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
	"week5-Microservice/hystrix/hystrix"
)

type serverHandler struct{}

func main() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	// hystrix
	totalThres := 50
	failThres := 0.8
	size := 10
	brokenDuration := 5 * time.Second
	rolling := hystrix.NewRollingWindow(size, totalThres, float64(failThres), brokenDuration)
	server := http.Server{Addr: ":8080"}

	go func() {
		rolling.Start()
		rolling.Monitor()
		rolling.ShowBrokenStatus()
	}()
	//go func() {
	//	time.Sleep(time.Second * 5)
	//	rolling.SetBrokenStatus()
	//}()
	go func() {
		http.HandleFunc("/visit", func(w http.ResponseWriter, r *http.Request) {
			status := http.StatusOK
			var str string
			if rolling.GetBrokenStatus() {
				status = http.StatusInternalServerError
				return
			}
			if status != http.StatusOK {
				rolling.SaveReqResult(true)
				str = "denied by hystrix"
			} else {
				rolling.SaveReqResult(false)
			}
			fmt.Println(status)
			w.WriteHeader(status)
			w.Write([]byte(str))
		})
		//var server serverHandler
		log.Fatalln(server.ListenAndServe())
	}()

	for {
		select {
		case <-sig:
			rolling.ShowAllBuckets()
			server.Shutdown(ctx)
			cancel()
		}
	}
}

//func (s *serverHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
//	log.Printf("%s\n", r.URL.Path)
//}
