package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"sync"
)

func main() {
	wg := sync.WaitGroup{}
	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func() {
			Client()
			wg.Done()
		}()
	}
	wg.Wait()
}

func Client() {
	res, err := http.Get("http://localhost:8080/visit")
	if err != nil {
		log.Println(err)
		return
	}
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println(err)
		return
	}
	log.Printf("%d: %s", res.StatusCode, string(data))
}
