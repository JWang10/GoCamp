# GRPC Microservice Project

## Description

把單體應用改成微服務應用，以簡易商品系統為主體。

商品系統將會有3個微服務應用和1個API Gateway，架構如下

![](assets/go-grpc-project.png)

- API Gateway：處理HTTP請求
- Auth服務：提供**註冊、登入**和**token**功能
- Product服務：提供**商品加入、庫存扣減**和**查找**功能
- Order服務：提供**創建訂單**功能

每個服務將會有相對應的 **postgreSQL** Database

![image-20220925170619785](assets/image-20220925170619785.png)

## Notice

- 為了方便後來建構使用，我們採用 `Makefile` 方式

  ```makefile
  # build protos from the correspond folder 
  proto:
      protoc pkg/**/pb/*.proto --go_out=. --go-grpc_out=.
   
  server:
      go run cmd/main.go
   
  ```

- 使用`viper`讀取`env`設定檔，採用`env`獨立設定

## API Gateway

### Module

```go
$ go get github.com/gin-gonic/gin github.com/spf13/viper google.golang.org/grpc
```

### Folder Structure

```go
api-gateway/
├── Makefile
├── cmd
│   └── main.go
├── go.mod
├── go.sum
└── pkg
    ├── auth
    │   ├── client.go
    │   ├── middleware.go
    │   ├── pb
    │   │   └── auth.proto
    │   ├── routes
    │   │   ├── login.go
    │   │   └── register.go
    │   └── routes.go
    ├── config
    │   ├── config.go
    │   └── envs
    │       └── dev.env
    ├── order
    │   ├── client.go
    │   ├── pb
    │   │   └── order.proto
    │   ├── routes
    │   │   └── create_order.go
    │   └── routes.go
    └── product
        ├── client.go
        ├── pb
        │   └── product.proto
        ├── routes
        │   ├── create_product.go
        │   └── find_one.go
        └── routes.go
```

### Definition of Proto

> 此處只列出service部分，細節請查看相對應proto

- Auth Service
  - Register
  - Login
  - Validate （with JWT）

```protobuf
service AuthService {
  rpc Register(RegisterRequest) returns (RegisterResponse) {}
  rpc Login(LoginRequest) returns (LoginResponse) {}
  rpc Validate(ValidateRequest) returns (ValidateResponse) {}
}
// Register
message RegisterRequest {
  string email = 1;
  string password = 2;
}
message RegisterResponse {
  int64 status = 1;
  string error = 2;
}
```

- Order Service
  - Create Order

```protobuf
service OrderService {
  rpc CreateOrder(CreateOrderRequest) returns (CreateOrderResponse) {}
}
```

- Product Service
  - Create Product
  - Find a Product
  - Decrease Stock of Product

```protobuf
service ProductService {
  rpc CreateProduct(CreateProductRequest) returns (CreateProductResponse) {}
  rpc FindOne(FindOneRequest) returns (FindOneResponse) {}
  rpc DecreaseStock(DecreaseStockRequest) returns (DecreaseStockResponse) {}
}
```

### Env Config

```sh
PORT=:3000
AUTH_SVC_URL=localhost:50051
PRODUCT_SVC_URL=localhost:50052
ORDER_SVC_URL=localhost:50053
```

### Feature

#### Register Route

- 用戶註冊帳戶，需要把請求發向`API Gateway`，Gateway接到請求後轉到Auth服務。因為知道了接受和響應的格式內容，所以我們建立一個結構體`RegisterRequest`綁定http請求和grpc請求的個體

```go
type RegisterRequest struct {
    Email    string `json:"email"`
    Password string `json:"password"`
}
 
func Register(ctx *gin.Context, c pb.AuthServiceClient) {
    body := RegisterRequest{}
 
    if err := ctx.BindJSON(&body); err != nil {
        ctx.AbortWithError(http.StatusBadRequest, err)
        return
    }
 
    ret, err := c.Register(context.Background(), &pb.RegisterRequest{
        Email:    body.Email,
        Password: body.Password,
    })
 
    if err != nil {
        ctx.AbortWithError(http.StatusBadGateway, err)
        return
    }
    ctx.JSON(int(res.Status), &ret)
}
```

#### Login Route

- 綁定http請求，轉發請求給auth服務
- 註冊用戶將會得到一組`JWT token`
  - 後續服務將需要帶上此 token

```go
type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func Login(ctx *gin.Context, c pb.AuthServiceClient) {
	body := LoginRequest{}

	if err := ctx.BindJSON(&body); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	ret, err := c.Login(context.Background(), &pb.LoginRequest{
		Email:    body.Email,
		Password: body.Password,
	})

	if err != nil {
		ctx.AbortWithError(http.StatusBadGateway, err)
		return
	}

	ctx.JSON(http.StatusCreated, &ret)
}
```

#### Auth Client

- 與後面的微服務應用進行溝通
- 與Product 和 Order Client相同

```go
type ServiceClient struct {
	Client pb.AuthServiceClient
}

func InitServiceClient(c *config.Config) pb.AuthServiceClient {
  // auth: AuthSvcUrl
  // product: ProductSvcUrl
  // order: OrderSvcUrl
	dail, err := grpc.Dial(c.AuthSvcUrl, grpc.WithInsecure())
	if err != nil {
		fmt.Println("Could not connect:", err)
	}
	return pb.NewAuthServiceClient(dail)
}
```

#### Auth Middleware

- 用來處理未認證請求，只允許註冊用戶訪問
- 透過請求`Header`攜帶`jwt token`，藉由auth服務驗證token是否正確

```go
 authorization := ctx.Request.Header.Get("authorization")
    if authorization == "" {
        ctx.AbortWithStatus(http.StatusUnauthorized)
        return
    }
 
    token := strings.Split(authorization, "Bearer ")
 
    if len(token) < 2 {
        ctx.AbortWithStatus(http.StatusUnauthorized)
        return
    }
 
    ret, err := c.svc.Client.Validate(context.Background(), &pb.ValidateRequest{
        Token: token[1],
    })
 
    if err != nil || ret.Status != http.StatusOK {
        ctx.AbortWithStatus(http.StatusUnauthorized)
        return
    }
```

#### Product Route

- 與前面註冊和登入路由雷同，獲取請求，進行轉發到Product微服務應用

```go
type CreateProductRequest struct {
	Name  string `json:"name"`
	Stock int64  `json:"stock"`
	Price int64  `json:"price"`
}

func CreateProduct(ctx *gin.Context, c pb.ProductServiceClient) {
	body := CreateProductRequest{}

	if err := ctx.BindJSON(&body); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	res, err := c.CreateProduct(context.Background(), &pb.CreateProductRequest{
		Name:  body.Name,
		Stock: body.Stock,
		Price: body.Price,
	})

	if err != nil {
		ctx.AbortWithError(http.StatusBadGateway, err)
		return
	}

	ctx.JSON(http.StatusCreated, &res)
}
// Find a product
func FineOne(ctx *gin.Context, c pb.ProductServiceClient) {
    id, _ := strconv.ParseInt(ctx.Param("id"), 10, 32)
 
    res, err := c.FindOne(context.Background(), &pb.FindOneRequest{
        Id: int64(id),
    })
 
    if err != nil {
        ctx.AbortWithError(http.StatusBadGateway, err)
        return
    }
 
    ctx.JSON(http.StatusCreated, &res)
}
```

#### Order Route

- 與前面註冊和登入路由雷同，獲取請求，進行轉發到Order微服務應用

```go
type CreateOrderRequest struct {
	ProductId int64 `json:"productId"`
	Quantity  int64 `json:"quantity"`
}

func CreateOrder(ctx *gin.Context, c pb.OrderServiceClient) {
	body := CreateOrderRequest{}

	if err := ctx.BindJSON(&body); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	userId, _ := ctx.Get("userId")

	res, err := c.CreateOrder(context.Background(), &pb.CreateOrderRequest{
		ProductId: body.ProductId,
		Quantity:  body.Quantity,
		UserId:    userId.(int64),
	})

	if err != nil {
		ctx.AbortWithError(http.StatusBadGateway, err)
		return
	}

	ctx.JSON(http.StatusCreated, &res)
}

```

#### Router

- 前面說明了路由功能，此處我們提供路由設定
- auth

```go
func RegisterRoutes(r *gin.Engine, c *config.Config) *ServiceClient {
    svc := &ServiceClient{
        Client: InitServiceClient(c),
    }
 
    routes := r.Group("/auth")
    routes.POST("/register", svc.Register)
    routes.POST("/login", svc.Login)
 
    return svc
}
 
func (svc *ServiceClient) Register(ctx *gin.Context) {
    routes.Register(ctx, svc.Client)
}
 
func (svc *ServiceClient) Login(ctx *gin.Context) {
    routes.Login(ctx, svc.Client)
}
```

- product

```go
func RegisterRoutes(r *gin.Engine, c *config.Config, authSvc *auth.ServiceClient) {
    a := auth.InitAuthMiddleware(authSvc)
 
    svc := &ServiceClient{
        Client: InitServiceClient(c),
    }
 
    routes := r.Group("/product")
    routes.Use(a.AuthRequired)
    routes.POST("/", svc.CreateProduct)
    routes.GET("/:id", svc.FindOne)
}
 
func (svc *ServiceClient) FindOne(ctx *gin.Context) {
    routes.FineOne(ctx, svc.Client)
}
 
func (svc *ServiceClient) CreateProduct(ctx *gin.Context) {
    routes.CreateProduct(ctx, svc.Client)
}
```

- order

```go
func RegisterRoutes(r *gin.Engine, c *config.Config, authSvc *auth.ServiceClient) {
    a := auth.InitAuthMiddleware(authSvc)
 
    svc := &ServiceClient{
        Client: InitServiceClient(c),
    }
 
    routes := r.Group("/order")
    routes.Use(a.AuthRequired)
    routes.POST("/", svc.CreateOrder)
}
 
func (svc *ServiceClient) CreateOrder(ctx *gin.Context) {
    routes.CreateOrder(ctx, svc.Client)
}
```

#### Main

- 路由完成且註冊綁定後，我們還需要在啟動時候調用其註冊功能

```go
r := gin.Default()
authSvc := *auth.RegisterRoutes(r, &c)
product.RegisterRoutes(r, &c, &authSvc)
order.RegisterRoutes(r, &c, &authSvc)
r.Run(c.Port) // port from config
```

### Demo

![image-20220925181507780](assets/image-20220925181507780.png)

## Auth Service

### Module

```go
$ go get github.com/spf13/viper google.golang.org/grpc gorm.io/gorm gorm.io/driver/postgres golang.org/x/crypto/bcrypt github.com/golang-jwt/jwt/v4
```

### Folder Structure

```go
auth-svc/
├── Makefile
├── cmd
│   └── main.go
├── go.mod
├── go.sum
└── pkg
    ├── config
    │   ├── config.go
    │   └── envs
    │       └── dev.env
    ├── db
    │   └── db.go
    ├── models
    │   └── auth.go
    ├── pb
    │   └── auth.proto
    ├── services
    │   └── auth.go
    └── utils
        ├── hash.go
        └── jwt.go
```

### Definition of Proto

- Auth Service
  - Register
  - Login
  - Validate （with JWT）

```protobuf
service AuthService {
  rpc Register(RegisterRequest) returns (RegisterResponse) {}
  rpc Login(LoginRequest) returns (LoginResponse) {}
  rpc Validate(ValidateRequest) returns (ValidateResponse) {}
}
// Register
message RegisterRequest {
  string email = 1;
  string password = 2;
}
message RegisterResponse {
  int64 status = 1;
  string error = 2;
}
```

### Env Config

```sh
PORT=:50051
DB_URL="host=<HOST> user=<USER> password=<PASSWORD> dbname=order_svc port=<PORT> sslmode=disable"
JWT_SECRET_KEY=d3r322d1
```

### Feature

#### Auth Model

- 透過`db.AutoMigrte`方法在應用啟動時自動創建資料庫數據表

```go
type User struct {
    Id       int64  `json:"id" gorm:"primaryKey"`
    Email    string `json:"email"`
    Password string `json:"password"`
}
```

#### HelperFuntion

- Hash

  - 採用`bcypt`模組對密碼加密及驗證

  ```go
  func HashPassword(password string) string {
      bytes, _ := bcrypt.GenerateFromPassword([]byte(password), 8)
   
      return string(bytes)
  }
   
  func CheckPasswordHash(password string, hash string) bool {
      err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
   
      return err == nil
  }
  ```

- JWT

  - 透過`cliams`策略和`dev.env`內定義的密鑰產生和驗證`JWT token`

  ```go
  type JwtWrapper struct {
  	SecretKey       string
  	Issuer          string
  	ExpirationHours int64
  }
  
  type JwtClaims struct {
  	Id    int64
  	Email string
  	jwt.RegisteredClaims
  }
  
  func (w *JwtWrapper) GenerateToken(user models.User) (signedToken string, err error) {
  	claims := JwtClaims{
  		Id:    user.Id,
  		Email: user.Email,
  		RegisteredClaims: jwt.RegisteredClaims{
  			ExpiresAt: jwt.NewNumericDate(time.Now().Local().Add(time.Hour * time.Duration(w.ExpirationHours))),
  			Issuer:    w.Issuer,
  		},
  	}
  	// Notice, the SigningMethod
  	// There is a specific key type, like SigningMethodES256 is about "*ecdsa.PrivateKey"
  	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
  
  	signedToken, err = token.SignedString([]byte(w.SecretKey))
  
  	if err != nil {
  		return "", err
  	}
  
  	return signedToken, nil
  }
  ```

#### Auth Service

- auth微服務的業務邏輯
- API Gateway中建立的auth路由會轉到此處
- 如下實例`Register Route`

```go
type Server struct {
    H   db.Handler
    Jwt utils.JwtWrapper
    pb.UnimplementedAuthServiceServer
}
 
func (s *Server) Register(ctx context.Context, req *pb.RegisterRequest) (*pb.RegisterResponse, error) {
    var user models.User
 
    if result := s.H.DB.Where(&models.User{Email: req.Email}).First(&user); result.Error == nil {
        return &pb.RegisterResponse{
            Status: http.StatusConflict,
            Error:  "Email already exists",
        }, nil
    }
 
    user.Email = req.Email
    user.Password = utils.HashPassword(req.Password)
 
    s.H.DB.Create(&user)
 
    return &pb.RegisterResponse{
        Status: http.StatusCreated,
    }, nil
}
```

#### Main

```go
	h := db.Init(c.DBUrl)
	// 產生 token
	jwt := utils.JwtWrapper{
		SecretKey:       c.JWTSecretKey,
		Issuer:          "go-auth-svc",
		ExpirationHours: 24 * 1,
	}

	conn, err := net.Listen("tcp", c.Port)

	if err != nil {
		log.Fatalln("Failed to connect", err)
	}

	fmt.Println("Auth Service on:", c.Port)

	// 啟用 rpc server
	s := services.Server{
		H:   h,
		Jwt: jwt,
	}
	grpcServer := grpc.NewServer()
	pb.RegisterAuthServiceServer(grpcServer, &s)
	if err := grpcServer.Serve(conn); err != nil {
		log.Fatalln("Failed to serve:", err)
	}
```

### Demo

![image-20220925203800199](assets/image-20220925203800199.png)

## Product Service

### Module

```go
$ go get github.com/gin-gonic/gin github.com/spf13/viper google.golang.org/grpc gorm.io/gorm
```

### Folder Structure

```go
product-svc/
├── Makefile
├── cmd
│   └── main.go
├── go.mod
├── go.sum
└── pkg
    ├── config
    │   ├── config.go
    │   └── envs
    │       └── dev.env
    ├── db
    │   └── db.go
    ├── models
    │   ├── product.go
    │   └── stock_decrease_log.go
    ├── pb
    │   └── product.proto
    └── services
        └── product.go
```

### Definition of Proto

- Product Service
  - Create Product
  - Find a Product
  - Decrease Stock of Product 

```protobuf
service ProductService {
  rpc CreateProduct(CreateProductRequest) returns (CreateProductResponse) {}
  rpc FindOne(FindOneRequest) returns (FindOneResponse) {}
  rpc DecreaseStock(DecreaseStockRequest) returns (DecreaseStockResponse) {}
}
```

### Env Config

```go
PORT=:50052
DB_URL="host=<HOST> user=<USER> password=<PASSWORD> dbname=order_svc port=<PORT> sslmode=disable"
```

### Feature

#### Product Model

- 因為冪等性質考慮，我們需要記錄所有減少的庫存

```go
type Product struct {
	Id                int64            `json:"id" gorm:"primaryKey"`
	Name              string           `json:"name"`
	Stock             int64            `json:"stock"`
	Price             int64            `json:"price"`
	StockDecreaseLogs StockDecreaseLog `gorm:"foreignKey:ProductRefer"`
}
type StockDecreaseLog struct {
	Id           int64 `json:"id" gorm:"primaryKey"`
	OrderId      int64 `json:"order_id"`
	ProductRefer int64 `json:"product_id"`
}
```

#### Product Service

- 此處我們需要處理所有進入的grpc請求
- `DecreaseStock`需要特別留意
  - 我們檢查了訂單ID已減去的庫存來保證數據一致性

```go
func (s *Server) DecreaseStock(ctx context.Context, req *pb.DecreaseStockRequest) (*pb.DecreaseStockResponse, error) {
	var product models.Product

	if ret := s.H.DB.First(&product, req.Id); ret.Error != nil {
		return &pb.DecreaseStockResponse{
			Status: http.StatusNotFound,
			Error:  ret.Error.Error(),
		}, nil
	}

	if product.Stock <= 0 {
		return &pb.DecreaseStockResponse{
			Status: http.StatusConflict,
			Error:  ErrOutofStock.Error(),
		}, nil
	}

	var log models.StockDecreaseLog

	// check the stock of product is same number (Idempotence)
	if ret := s.H.DB.Where(&models.StockDecreaseLog{OrderId: req.OrderId}).First(&log); ret.Error == nil {
		return &pb.DecreaseStockResponse{
			Status: http.StatusConflict,
			Error:  ErrAlreadyDecreased.Error(),
		}, nil
	}

	// product decrease
	product.Stock -= 1

	s.H.DB.Save(&product)

	log.OrderId = req.OrderId
	log.ProductRefer = product.Id // record the product's ID after decreased

	s.H.DB.Create(&log) // record the changelog

	return &pb.DecreaseStockResponse{
		Status: http.StatusOK,
	}, nil
}
```

#### Main

- 與auth service相同
- 建立DB和grpc啟動連結

## Order Service

### Module

```go
$ go get github.com/gin-gonic/gin github.com/spf13/viper google.golang.org/grpc gorm.io/gorm
```

### Folder Structure

```go
order-svc/
├── Makefile
├── cmd
│   └── main.go
├── go.mod
├── go.sum
└── pkg
    ├── client
    │   └── product_client.go
    ├── config
    │   ├── config.go
    │   └── envs
    │       └── dev.env
    ├── db
    │   └── db.go
    ├── models
    │   └── order.go
    ├── pb
    │   ├── order.proto
    │   └── product.proto
    └── services
        └── order.go
```

### Definition of Proto

- Order Service
  - Create Order

```protobuf
service OrderService {
  rpc CreateOrder(CreateOrderRequest) returns (CreateOrderResponse) {}
}
message CreateOrderRequest {
  int64 productId = 1;
  int64 quantity = 2;
  int64 userId = 3;
}
message CreateOrderResponse {
  int64 status = 1;
  string error = 2;
  int64 id = 3;
}
```

- Product Service
  - 因為我們建立訂單需要商品訊息，需要調用product微服務
    - 用來檢查商品是否存在
    - 根據訂單請求減少商品庫存

```protobuf
service ProductService {
  rpc CreateProduct(CreateProductRequest) returns (CreateProductResponse){}
  rpc FindOne(FindOneRequest) returns (FindOneResponse){}
  rpc DecreaseStock(DecreaseStockRequest) returns (DecreaseStockResponse){}
}
message CreateProductRequest {
  string name = 1;
  int64 stock = 2;
  int64 price = 3;
}
message DecreaseStockRequest {
  int64 id = 1;
  int64 orderId = 2;
}
```

Env Config

```sh
PORT=:50053
DB_URL="host=<HOST> user=<USER> password=<PASSWORD> dbname=order_svc port=<PORT> sslmode=disable"
PRODUCT_SVC_URL=localhost:50052
```



### Feature

#### Order Model

```go
type Order struct {
	Id        int64 `json:"id" gorm:"primaryKey"`
	Price     int64 `json:"price"`
	ProductId int64 `json:"product_id"`
	UserId    int64 `json:"user_id"`
}
```

#### Product Client

- 前面提到需要調用product微服務，就需要建立一個client

```go

type ProductServiceClient struct {
	Client pb.ProductServiceClient
}

func InitProductServiceClient(url string) ProductServiceClient {
	cc, err := grpc.Dial(url, grpc.WithInsecure())

	if err != nil {
		log.Fatalln("Could not connect", err)
	}

	c := ProductServiceClient{
		Client: pb.NewProductServiceClient(cc),
	}
	return c
}

func (c *ProductServiceClient) FindOne(productId int64) (*pb.FindOneResponse, error) {
	req := &pb.FindOneRequest{Id: productId}
	return c.Client.FindOne(context.Background(), req)
}

func (c *ProductServiceClient) DecreaseStock(productId int64, orderId int64) (*pb.DecreaseStockResponse, error) {
	req := &pb.DecreaseStockRequest{
		Id:      productId,
		OrderId: orderId,
	}
	return c.Client.DecreaseStock(context.Background(), req)
}
```

#### Order Service

```go
type Server struct {
	H          db.Handler
	ProductSvc client.ProductServiceClient
	pb.UnimplementedOrderServiceServer
}

var (
	ErrNotEnough = errors.New("stock is not enough")
)

func (s *Server) CreateOrder(ctx context.Context, req *pb.CreateOrderRequest) (*pb.CreateOrderResponse, error) {
  // product_client的FindOne方法去調用product微服務
	product, err := s.ProductSvc.FindOne(req.ProductId)

	if err != nil {
		return &pb.CreateOrderResponse{
			Status: http.StatusBadRequest,
			Error:  err.Error(),
		}, nil
	} else if product.Status >= http.StatusNotFound {
		return &pb.CreateOrderResponse{
			Status: product.Status,
			Error:  product.Error,
		}, nil
	} else if product.Data.Stock < req.Quantity {
		return &pb.CreateOrderResponse{
			Status: http.StatusConflict,
			Error:  ErrNotEnough.Error(),
		}, nil
	}

	order := models.Order{
		Price:     product.Data.Price,
		ProductId: product.Data.Id,
		UserId:    req.UserId,
	}
	// 建立訂單
	s.H.Db.Create(&order)
	// 庫存扣減
	ret, err := s.ProductSvc.DecreaseStock(req.ProductId, order.Id)

	if err != nil {
		return &pb.CreateOrderResponse{
			Status: http.StatusBadRequest,
			Error:  err.Error(),
		}, nil
	} else if ret.Status == http.StatusConflict {
		// Conflict - same order
		s.H.Db.Delete(&models.Order{}, order.Id)
		return &pb.CreateOrderResponse{
			Status: http.StatusConflict,
			Error:  err.Error(),
		}, nil
	}
	return &pb.CreateOrderResponse{
		Status: http.StatusCreated,
		Id:     order.Id,
	}, nil
}
```

#### Main

- 與auth service相同
- 建立DB和grpc啟動連結

```go
h := db.Init(c.DBUrl)

	conn, err := net.Listen("tcp", c.Port)

	if err != nil {
		log.Fatalln("Failed to connect", err)
	}
	// 初始化 product_client，建立跟product微服務連結
	productSvc := client.InitProductServiceClient(c.ProductSvcUrl)

	fmt.Println("Order Service on", c.Port)

	s := services.Server{
		H:          h,
		ProductSvc: productSvc,
	}
	grpcServer := grpc.NewServer()
	pb.RegisterOrderServiceServer(grpcServer, &s)

	if err := grpcServer.Serve(conn); err != nil {
		log.Fatalln("Failed to serve", err)
	}
```

## Issue

- Hash key is invalid type
  - `ecdsa`加密方法，現在已改成需要使用`PrivateKey`結構體格式，而不是`[]byte`
  - 

```go
Auth Service on: :50051
2022/09/25 10:47:56 r43t18sc
2022/09/25 10:47:56 
 key is of invalid type
// ----
// Get the key
var ecdsaKey *ecdsa.PrivateKey
switch k := key.(type) {
  case *ecdsa.PrivateKey:
  ecdsaKey = k
  default:
  return "", ErrInvalidKeyType
}
// ----
type PrivateKey struct {
	PublicKey
	D *big.Int
}
PrivateKey represents an ECDSA private key.
```

- Nil pointer issue

  ```go
  panic: runtime error: invalid memory address or nil pointer dereference
  ```

  - 沒有初始化，直接調用會出現

  - 判斷key元素不存在，返回nil沒有處理也會出現
    - 如下面map例子

  ```go
  // 當key元素不存在時，返回value預設值(零值)，剛好是*MConny預設值是nil，沒有處理就會發生nil point error
  var mMap map[string]*MConn
      m1 := mMap["name"]
      m1.Name = "qqq"
  // ----
  var mMap map[string]*MConn
      m1, ok := mMap["name"]
      if ok {
          m1.Name = "qqq"
      }
  ```

## Demo

![iShot_2022-09-25_23.22.44](assets/iShot_2022-09-25_23.22.44.png)

![iShot_2022-09-25_23.24.46](assets/iShot_2022-09-25_23.24.46.png)

![iShot_2022-09-25_16.35.17](assets/iShot_2022-09-25_16.35.17.png)
