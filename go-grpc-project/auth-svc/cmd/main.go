package main

import (
	"auth-svc/pkg/config"
	"auth-svc/pkg/db"
	"auth-svc/pkg/pb"
	"auth-svc/pkg/services"
	"auth-svc/pkg/utils"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main() {
	c, err := config.LoadConfig()

	if err != nil {
		log.Fatalln("Failed at config", err)
	}

	h := db.Init(c.DBUrl)

	jwt := utils.JwtWrapper{
		SecretKey:       c.JWTSecretKey,
		Issuer:          "go-auth-svc",
		ExpirationHours: 24 * 1,
	}

	conn, err := net.Listen("tcp", c.Port)

	if err != nil {
		log.Fatalln("Failed to connect", err)
	}

	fmt.Println("Auth Service on:", c.Port)

	s := services.Server{
		H:   h,
		Jwt: jwt,
	}
	grpcServer := grpc.NewServer()

	pb.RegisterAuthServiceServer(grpcServer, &s)

	if err := grpcServer.Serve(conn); err != nil {
		log.Fatalln("Failed to serve:", err)
	}
}
