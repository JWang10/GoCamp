package utils

import (
	"auth-svc/pkg/models"
	"errors"
	"github.com/golang-jwt/jwt/v4"
	"time"
)

type JwtWrapper struct {
	SecretKey       string
	Issuer          string
	ExpirationHours int64
}

type JwtClaims struct {
	Id    int64
	Email string
	jwt.RegisteredClaims
}

func (w *JwtWrapper) GenerateToken(user models.User) (signedToken string, err error) {
	claims := JwtClaims{
		Id:    user.Id,
		Email: user.Email,
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Local().Add(time.Hour * time.Duration(w.ExpirationHours))),
			Issuer:    w.Issuer,
		},
	}

	// Notice, the SigningMethod
	// There is a specific key type, like SigningMethodES256 is about "*ecdsa.PrivateKey"
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	signedToken, err = token.SignedString([]byte(w.SecretKey))

	if err != nil {
		return "", err
	}

	return signedToken, nil
}

func (w *JwtWrapper) ValidateToken(signedToken string) (claims *JwtClaims, err error) {
	token, err := jwt.ParseWithClaims(
		signedToken, &JwtClaims{},
		func(token *jwt.Token) (any, error) {
			return []byte(w.SecretKey), nil
		},
	)

	if err != nil {
		return
	}

	claims, ok := token.Claims.(*JwtClaims)

	if !ok {
		return nil, errors.New("couldn't parse claims")
	}

	if claims.ExpiresAt.Unix() < time.Now().Local().Unix() {
		return nil, errors.New("JWT token is expired")
	}

	return claims, nil
}
