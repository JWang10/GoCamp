package main

import (
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
	"order-svc/pkg/client"
	"order-svc/pkg/config"
	"order-svc/pkg/db"
	"order-svc/pkg/pb"
	"order-svc/pkg/services"
)

func main() {
	c, err := config.LoadConfig()

	if err != nil {
		log.Fatalln("Failed at config", err)
	}

	h := db.Init(c.DBUrl)

	conn, err := net.Listen("tcp", c.Port)

	if err != nil {
		log.Fatalln("Failed to connect", err)
	}

	productSvc := client.InitProductServiceClient(c.ProductSvcUrl)

	fmt.Println("Order Service on", c.Port)

	s := services.Server{
		H:          h,
		ProductSvc: productSvc,
	}
	grpcServer := grpc.NewServer()
	pb.RegisterOrderServiceServer(grpcServer, &s)

	if err := grpcServer.Serve(conn); err != nil {
		log.Fatalln("Failed to serve", err)
	}
}
