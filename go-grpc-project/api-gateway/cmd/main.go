package main

import (
	"github.com/gin-gonic/gin"
	"go-grpc-project/pkg/auth"
	"go-grpc-project/pkg/config"
	"go-grpc-project/pkg/order"
	"go-grpc-project/pkg/product"
	"log"
)

func main() {
	//ctx := context.Background()
	//ctx, cancel := context.WithCancel(ctx) // call WithCancel() -> cancel() to close the context
	//eg, egCtx := errgroup.WithContext(ctx) // errgroup control goroutine
	//
	//// listen linux signal
	//sig := make(chan os.Signal, 1)
	//signal.Notify(sig, os.Interrupt) // =syscall.SIGINT

	// load config
	c, err := config.LoadConfig()

	if err != nil {
		log.Fatalln("Failed at config", err)
	}

	r := gin.Default()

	authSvc := *auth.RegisterRoutes(r, &c)
	product.RegisterRoutes(r, &c, &authSvc)
	order.RegisterRoutes(r, &c, &authSvc)

	r.Run(c.Port)

	//eg.Go(func() error {
	//	return r.Run(c.Port)
	//})
	//
	//// graceful exit
	//eg.Go(func() error {
	//	// listen signal: ctrl+c or kill -9
	//	for {
	//		select {
	//		case <-egCtx.Done():
	//			return egCtx.Err()
	//		case <-sig: // ctrl+c or kill -9
	//			cancel()
	//		}
	//	}
	//})
	//
	//// blocks until all function calls from the Go method have returned
	//if err := eg.Wait(); err != nil {
	//	fmt.Println("error: ", err)
	//}
}
