package product

import (
	"fmt"
	"go-grpc-project/pkg/config"
	"go-grpc-project/pkg/product/pb"
	"google.golang.org/grpc"
)

type ServiceClient struct {
	Client pb.ProductServiceClient
}

func InitServiceClient(c *config.Config) pb.ProductServiceClient {
	cc, err := grpc.Dial(c.ProductSvcUrl, grpc.WithInsecure())

	if err != nil {
		fmt.Println("Counld not connect:", err)
	}

	return pb.NewProductServiceClient(cc)
}
