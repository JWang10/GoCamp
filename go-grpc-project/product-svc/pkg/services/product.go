package services

import (
	"context"
	"errors"
	"net/http"
	"product-svc/pkg/db"
	"product-svc/pkg/models"
	"product-svc/pkg/pb"
)

type Server struct {
	H db.Handler
	pb.UnimplementedProductServiceServer
}

var (
	ErrOutofStock       = errors.New("Out of stock")
	ErrAlreadyDecreased = errors.New("Stock already decreased")
)

func (s *Server) CreateProduct(ctx context.Context, req *pb.CreateProductRequest) (*pb.CreateProductResponse, error) {
	var product models.Product

	product.Name = req.Name
	product.Price = req.Price
	product.Stock = req.Stock

	if ret := s.H.DB.Create(&product); ret.Error != nil {
		return &pb.CreateProductResponse{
			Status: http.StatusConflict,
			Error:  ret.Error.Error(),
		}, nil
	}

	return &pb.CreateProductResponse{
		Status: http.StatusOK,
		Id:     product.Id,
	}, nil
}

func (s *Server) FindOne(ctx context.Context, req *pb.FindOneRequest) (*pb.FindOneResponse, error) {
	var product models.Product

	if ret := s.H.DB.First(&product, req.Id); ret.Error != nil {
		return &pb.FindOneResponse{
			Status: http.StatusNotFound,
			Error:  ret.Error.Error(),
		}, nil
	}

	data := &pb.FindOneData{
		Id:    product.Id,
		Name:  product.Name,
		Price: product.Price,
		Stock: product.Stock,
	}

	return &pb.FindOneResponse{
		Status: http.StatusOK,
		Data:   data,
	}, nil
}

func (s *Server) DecreaseStock(ctx context.Context, req *pb.DecreaseStockRequest) (*pb.DecreaseStockResponse, error) {
	var product models.Product

	if ret := s.H.DB.First(&product, req.Id); ret.Error != nil {
		return &pb.DecreaseStockResponse{
			Status: http.StatusNotFound,
			Error:  ret.Error.Error(),
		}, nil
	}

	if product.Stock <= 0 {
		return &pb.DecreaseStockResponse{
			Status: http.StatusConflict,
			Error:  ErrOutofStock.Error(),
		}, nil
	}

	var log models.StockDecreaseLog

	// check the stock of product is same number (Idempotence)
	if ret := s.H.DB.Where(&models.StockDecreaseLog{OrderId: req.OrderId}).First(&log); ret.Error == nil {
		return &pb.DecreaseStockResponse{
			Status: http.StatusConflict,
			Error:  ErrAlreadyDecreased.Error(),
		}, nil
	}

	// product decrease
	product.Stock -= 1

	s.H.DB.Save(&product)

	log.OrderId = req.OrderId
	log.ProductRefer = product.Id // record the product's ID after decreased

	s.H.DB.Create(&log) // record the changelog

	return &pb.DecreaseStockResponse{
		Status: http.StatusOK,
	}, nil
}
