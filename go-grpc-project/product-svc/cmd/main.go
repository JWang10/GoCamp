package main

import (
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
	"product-svc/pkg/config"
	"product-svc/pkg/db"
	"product-svc/pkg/pb"
	"product-svc/pkg/services"
)

func main() {
	c, err := config.LoadConfig()

	if err != nil {
		log.Fatalln("Failed at config", err)
	}

	h := db.Init(c.DBUrl)
	conn, err := net.Listen("tcp", c.Port)

	if err != nil {
		log.Fatalln("Failed to connect", err)
	}

	fmt.Println("Product Service on", c.Port)

	s := services.Server{H: h}
	grpcServer := grpc.NewServer()
	pb.RegisterProductServiceServer(grpcServer, &s)

	if err := grpcServer.Serve(conn); err != nil {
		log.Fatalln("Failed to serve", err)
	}

}
