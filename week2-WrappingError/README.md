# Wrapping errors

如果我們進行DB操作，遇到`sql.ErrNoRows`問題，是否需要`Wrap`這個error給上一層？

- 用以下的例子來實作

```go
func main() {
	// 假設我們query db，返回not found error
	err := Query()
	if err != nil {
		fmt.Printf("got err: %+v\n", err)
	}
}

func Query() error {
	return sql.ErrNoRows
}
//outputs:
//got unknown error: sql: no rows in result set
```

在實際場景中，可能會需要下面一些情況

- 需要根據返回類型做不同的處理 - 情境A

- 需要為錯誤增加上下文，便於調用者了解錯誤場景訊息 - 情境B

遇到這些情況，我們會需要將error包裝給上層去做而外處理。若直接在底層做處理，那遇到其他擴展情況將會花費較大的成本來調整。

```go
func main() {
	// 假設我們query db，返回not found error
	err := WrapQuery()
  // 情境A：根據不同返回類型做處理
	if err == sql.ErrNoRows {
		fmt.Printf("data not found, %+v\n", err)
		return

	}
  // 情境A： unknown error
	if err != nil {
		fmt.Printf("got unknown error: %+v\n", err)
	}
}

func Query() error {
  //情境B：為錯誤增加上下文
	//return sql.ErrNoRows
	return fmt.Errorf("query err: %+v\n", sql.ErrNoRows)
}

func WrapQuery() error {
	return Query()
}
//outputs
//情境A
//data not found, sql: no rows in result set
//情境B
//got unknown error: query err: sql: no rows in result set
```

這時候，我們發現`err == sql.ErrNoRows`將不會等值。除此之外，在目前Go版本中，在返回錯誤時，都丟失了`堆棧訊息`。

## 解決方案

- Go 1.13內置了`xerrors`, 但不支援調用棧訊息

- `golang.org/x/xerrors`包是社區在Go2基於錯誤值處理的提案

  - `Error‘s %w`整合進`fmt.Errorf`

  - 目前版本兼容以前Go版本

  - 雖然`FormatError`也加入調用棧訊息，但是使用上比下面方法複雜一些

    ```go
    type Formatter interface {
    	error
      // 列印收到的第一個error，返回在錯誤鏈中的下一個error
    	FormatError(p Printer) (next error)
    }
    // 返回當前調用棧的caller
    func Caller(skip int) Frame
    // 列印錯誤訊息
    func (f Frame) Format(p Printer)
    ```

    

- github.com/pkg/errors （我們此project採用這方案）

  - `Wrap`方法用來包裝底層錯誤，增加上下文訊息，並加入調用棧
  - `Cause`用來判斷底層錯誤
  - `WithMessage`僅增加上下文，跟`Wrap`不同的是，不加入調用棧

```go
func main() {
	// 假設我們query db，返回not found error
	err := WrapQuery()
	if errors.Cause(err) == sql.ErrNoRows {
		fmt.Printf("data not found, %+v\n", err)
		return
	}
	if err != nil {
		fmt.Printf("got unknown error: %+v\n", err)
	}
}

func Query() error {
	return errors.Wrap(sql.ErrNoRows, "query error")
}

func WrapQuery() error {
	return errors.WithMessage(Query(), "WrapQuery failed")
}
/*
data not found, sql: no rows in result set
query error
main.Query
        /Workspace/GoPractice/Projects/GoCamp/week2-WrappingError/pkg-errors/main.go:24
main.WrapQuery
        /Workspace/GoPractice/Projects/GoCamp/week2-WrappingError/pkg-errors/main.go:28
main.main
        /Workspace/GoPractice/Projects/GoCamp/week2-WrappingError/pkg-errors/main.go:11
runtime.main
        /Users/jwang/sdk/go1.18.1/src/runtime/proc.go:250
runtime.goexit
        /Users/jwang/sdk/go1.18.1/src/runtime/asm_arm64.s:1259
WrapQuery failed
*/
```

## 心得

如果只是作為`lib`進行開發，包裝後的作法是可以理解的。因為當在使用`errors.Cause(err, sql.ErrNoRows)`，也就就表示這個`sql.ErrNoRows`的細節被暴露出來了。

而對於在設計API來說，這類問題也是需要考量的因素。一般可能會而外定義一個錯誤類型去整合，並從其中衍生出實例。