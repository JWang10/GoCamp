package main

import (
	"database/sql"
	"fmt"
	"github.com/pkg/errors"
)

func main() {
	// 假設我們query db，返回not found error
	err := WrapQuery()
	if errors.Is(errors.Cause(err), sql.ErrNoRows) {
		fmt.Printf("data not found, %+v\n", err)
		//fmt.Printf("%+v\n", err)
		return

	}
	if err != nil {
		fmt.Printf("got unknown error: %+v\n", err)
	}
}

func Query() error {
	return errors.Wrap(sql.ErrNoRows, "query error")
}

func WrapQuery() error {
	return errors.WithMessage(Query(), "WrapQuery failed")
}
