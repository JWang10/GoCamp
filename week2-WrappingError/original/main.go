package main

import (
	"database/sql"
	"fmt"
)

func main() {
	// 假設我們query db，返回not found error
	err := WrapQuery()
	if err == sql.ErrNoRows {
		fmt.Printf("data not found, %+v\n", err)
		return

	}
	if err != nil {
		fmt.Printf("got unknown error: %+v\n", err)
	}
}

func Query() error {
	return fmt.Errorf("query err: %+v\n", sql.ErrNoRows)
}

func WrapQuery() error {
	return Query()
}
