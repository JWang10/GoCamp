package fix_length

import (
	"fmt"
	"log"
	"net"
)

func ServerFixLength(conn net.Conn) {
	fmt.Println("server, fix length")

	buf := make([]byte, 1024) // ordinary buffer = 4096 byte,
	for {
		_, err := conn.Read(buf)
		checkErr(err)
		fmt.Println(string(buf))
	}
}
func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
