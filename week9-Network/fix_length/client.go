package fix_length

import "net"

func ClientFixLength(conn net.Conn) {
	b := 1024
	buff := make([]byte, b)
	msg := "fix_length"
	temp := []byte(msg)
	for i := 0; i < len(temp) && i < b; i++ {
		buff[i] = temp[i]
	}
	// send 10 times messages
	for i := 0; i < 10; i++ {
		_, err := conn.Write(buff) // ordinary: []byte(msg)
		checkErr(err)
	}
}
