package protocal

import (
	"encoding/binary"
	"errors"
	"fmt"
)

/*
## Protocol from goim

| Name             |       Length        | Remork                           |
| ---------------- | :-----------------: | -------------------------------- |
| Package Length   |       4 bytes       | header + body length             |
| Header Length    |       2 bytes       | protocol header length           |
| Protocol Version |       2 bytes       | protocol version                 |
| Operation        |       4 bytes       | operation for request            |
| Sequence id      |       4 bytes       | sequence number chosen by client |
| Body             | PackLen + HeaderLen | binary body bytes                |
*/

const (
	PackageLengthBytes = 4
	HeaderLengthBytes  = 2
	VersionBytes       = 2
	OperationBytes     = 4
	SequenceIDBytes    = 4

	HeaderLength = PackageLengthBytes + HeaderLengthBytes + VersionBytes + OperationBytes + SequenceIDBytes
)

var (
	ErrFormat = errors.New("wrong format length, <16")
)

func GoIMDecoder(data []byte) (any, error) {
	if len(data) < HeaderLength {
		return nil, ErrFormat
	}
	packageLen := binary.BigEndian.Uint32(data[:4])
	fmt.Println("package length:", packageLen)
	headerLen := binary.BigEndian.Uint16(data[4:6])
	fmt.Println("header length:", headerLen)
	version := binary.BigEndian.Uint16(data[6:8])
	fmt.Println("version:", version)
	operation := binary.BigEndian.Uint32(data[8:12])
	fmt.Println("operation:", operation)
	sequenceId := binary.BigEndian.Uint32(data[12:16])
	fmt.Println("sequenceId:", sequenceId)

	payload := string(data[16:])
	fmt.Println("payload:", payload)
	return payload, nil
}

func GoIMEncoder(data []byte) (any, error) {
	headerLen := HeaderLength
	packageLen := headerLen + len(data)

	newData := make([]byte, packageLen)
	newData = append(newData)
	binary.BigEndian.PutUint32(newData[:4], uint32(packageLen))
	binary.BigEndian.PutUint16(newData[4:6], uint16(headerLen))

	version := 1
	operation := 2
	sequenceId := 3
	binary.BigEndian.PutUint16(newData[6:8], uint16(version))
	binary.BigEndian.PutUint32(newData[8:12], uint32(operation))
	binary.BigEndian.PutUint32(newData[12:16], uint32(sequenceId))

	//newData = append(newData, data...)
	copy(newData[16:], data)
	return newData, nil
}
