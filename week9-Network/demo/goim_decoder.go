package main

import (
	"fmt"
	"log"
	"week9-Network/protocal"
)

func main() {
	msg := "test"
	encoder, err := protocal.GoIMEncoder([]byte(msg))
	fmt.Printf("%v\n", encoder)
	checkErr(err)
	decoder, err := protocal.GoIMDecoder(encoder.([]byte))
	checkErr(err)
	fmt.Printf("%v\n", decoder)

}
func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
