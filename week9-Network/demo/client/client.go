package main

import (
	"fmt"
	"log"
	"net"
	"week9-Network/frame_decoder"
)

func main() {
	conn, err := net.Dial("tcp", ":77")
	//conn, err := net.Dial("udp", ":77")
	checkErr(err)
	defer conn.Close()
	fmt.Println("connection success")

	//fix_length.ClientFixLength(conn)
	//delimiter_based.ClientDelimiterBased(conn)
	frame_decoder.ClientFrameDecoder(conn)
	//goim_decoder.ClientGoimDecoder(conn)
}

func checkErr(err error) {

	if err != nil {
		log.Fatal(err)
	}
}
