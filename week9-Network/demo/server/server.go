package main

import (
	"fmt"
	"log"
	"net"
	"week9-Network/frame_decoder"
)

func main() {
	sc, err := net.Listen("tcp", ":77")
	checkErr(err)
	defer sc.Close()
	for {
		conn, err := sc.Accept()
		checkErr(err)
		fmt.Println(conn.RemoteAddr().String(), "tcp connection success")
		//go fix_length.ServerFixLength(conn)
		//go delimiter_based.ServerDelimiterBased(conn)
		go frame_decoder.ServerFrameDecoder(conn)
		//go goim_decoder.ServerGoimDecoder(conn)
	}
}
func checkErr(err error) {

	if err != nil {
		log.Fatal(err)
	}
}
