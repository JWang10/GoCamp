package delimiter_based

import (
	"net"
)

func ClientDelimiterBased(conn net.Conn) {
	b := 1024
	buff := make([]byte, b)
	msg := "delimiter based\n"
	temp := []byte(msg)
	for i := 0; i < len(temp) && i < b; i++ {
		buff[i] = temp[i]
	}
	// send 10 times messages
	for i := 0; i < 10; i++ {
		_, err := conn.Write([]byte(msg)) // ordinary: []byte(msg)
		checkErr(err)
	}
}
