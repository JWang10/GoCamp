package delimiter_based

import (
	"bufio"
	"fmt"
	"log"
	"net"
)

func ServerDelimiterBased(conn net.Conn) {
	fmt.Println("server, delimiter based")

	reader := bufio.NewReader(conn)
	for {
		readString, err := reader.ReadString('\n')
		if err != nil {
			return
		}
		fmt.Print(readString)
	}
}
func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
