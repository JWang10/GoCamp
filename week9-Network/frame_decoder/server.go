package frame_decoder

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"net"
)

func ServerFrameDecoder(conn net.Conn) {
	fmt.Println("server, length field based frame decoder")

	reader := bufio.NewReader(conn)

	var payloadSize int32
	var err error
	// decode package for the length of data
	err = binary.Read(reader, binary.BigEndian, &payloadSize)
	checkErr(err)
	fmt.Println("payload ", payloadSize)
	// get payload size data
	buff := make([]byte, payloadSize)
	_, err = io.ReadFull(reader, buff)
	checkErr(err)
	fmt.Println(string(buff))
}
func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
