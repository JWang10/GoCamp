package frame_decoder

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"net"
)

func ClientFrameDecoder(conn net.Conn) {
	var err error
	var buff bytes.Buffer
	msg := "hi, length field based frame decoder"
	fmt.Println(int32(len(msg)))
	// write the length of data
	err = binary.Write(&buff, binary.BigEndian, int32(len(msg))) // ordinary: []byte(msg)
	checkErr(err)
	_, err = buff.Write([]byte(msg))
	checkErr(err)
	// send 10 times messages
	for i := 0; i < 10; i++ {
		_, err := conn.Write(buff.Bytes()) // ordinary: []byte(msg)
		checkErr(err)
	}
}
