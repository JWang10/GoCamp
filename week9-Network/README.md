# Socket

## Question

总结几种 socket 粘包的解包方式：fix length/delimiter based/length field based frame decoder。尝试举例其应用。
实现一个从 socket connection 中解码出 goim 协议的解码器。

## 為什麼會有粘包和半包問題？

**因為 TCP 是面向連線的傳輸協議，TCP 傳輸的資料是以流 (stream) 的形式，而流資料是沒有明確的開始結尾邊界，所以 TCP 也沒辦法判斷哪一段流屬於一個訊息**。

### 粘包的主要原因

- 傳送方每次寫入資料 < Socket 緩衝區大小
- 接收方讀取Socket 緩衝區資料不夠及時

### 半包的主要原因

- 傳送方每次寫入資料 > Socket 緩衝區大小
- 傳送的資料大於協議的 MTU (Maximum Transmission Unit，最大傳輸單元)，因此必須拆包。

## Socket 黏包的解包方式

### Fix length

- 雙方規定一個固定長度的buffer來接受資料
- 缺點為如果發送數據小於約定的buffer長度，就會造成數據冗余浪費；相反大於buffer長度，會造成數據接收不完整，導致半包問題

### Delimiter based

- 基於特定符號分隔邊界
- 缺點為數據量過大時候，因要掃描到特定符號，會造成性能開銷

### Length field based frame decoder

- 封裝Request協議：header + data payload
- 每次傳輸數據，將數據長度寫入到數據前面；每次讀取數據時，先將存放的長度取出，再讀取到buffer

## GoIM Protocal

| Name             |       Length        | Remork                           |
| ---------------- | :-----------------: | -------------------------------- |
| Package Length   |       4 bytes       | header + body length             |
| Header Length    |       2 bytes       | protocol header length           |
| Protocol Version |       2 bytes       | protocol version                 |
| Operation        |       4 bytes       | operation for request            |
| Sequence id      |       4 bytes       | sequence number chosen by client |
| Body             | PackLen + HeaderLen | binary body bytes                |

## Demo

### Fix length

- before

  ![image-20220828180419839](assets/image-20220828180419839.png)

- After

![image-20220828180658948](assets/image-20220828180658948.png)

### Delimiter based

- before

  - server use '\n' to distinguish the border of stream
  - the picture follow is without '\n' 

  ![image-20220828181454791](assets/image-20220828181454791.png)

- after

  ![image-20220828215440455](assets/image-20220828215440455.png)

### Length field based frame decoder 

![image-20220828221040861](assets/image-20220828221040861.png)

### GoIM decoder

![image-20220828233016332](assets/image-20220828233016332.png)
