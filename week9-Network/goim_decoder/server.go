package goim_decoder

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"net"
	"week9-Network/protocal"
)

func ServerGoimDecoder(conn net.Conn) {
	fmt.Println("server, goim decoder")

	reader := bufio.NewReader(conn)
	var header int32
	var err error
	err = binary.Read(reader, binary.BigEndian, &header)
	checkErr(err)
	buff := make([]byte, header-4)
	_, err = io.ReadFull(reader, buff)
	checkErr(err)
	fmt.Println(buff)
	body, err := protocal.GoIMDecoder(buff)
	result := fmt.Sprintf("%s", body)
	fmt.Println(result)
}

func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
