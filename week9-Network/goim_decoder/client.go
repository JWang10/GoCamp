package goim_decoder

import (
	"fmt"
	"net"
	"week9-Network/protocal"
)

func ClientGoimDecoder(conn net.Conn) {
	msg := "message through goim protocal"
	data, err := protocal.GoIMEncoder([]byte(msg))
	fmt.Printf("%v\n", data)
	checkErr(err)

	for i := 0; i < 10; i++ {
		_, err = conn.Write(data.([]byte))
		checkErr(err)
	}
}
